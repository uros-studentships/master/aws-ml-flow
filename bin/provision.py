from pathlib import Path
import os
import paramiko
import argparse


def _run_command(ssh_client, command, message=None, verbose=False):
    if message is not None:
        print(f'{message}...', flush=True, end='')
    stdin, stdout, stderr = ssh_client.exec_command(command)
    job_status = 'done' if stdout.channel.recv_exit_status() == 0 else 'failed'
    if message is not None:
        print(job_status)

    if verbose:
        print(stdout.read().decode())
        print(stderr.read().decode())
    
    return job_status


def _copy_files(ssh_client, files, sudo=False):
    with ssh_client.open_sftp() as sftp_client:
        for source_path, dest_path in files:
            # we don't need tilde: https://github.com/fabric/fabric/issues/1653#issuecomment-407220809
            dest_path_clean = dest_path if not dest_path.startswith('~') else dest_path[2:]

            print(f'copying [{source_path} -> {dest_path}]...', flush=True, end='')
            
            if not sudo:
                sftp_client.put(source_path, dest_path_clean)
                print('done')
            else:
                dest_filename = os.path.basename(dest_path)
                tmp_destination = os.path.join('/tmp', dest_filename)
                sftp_client.put(source_path, tmp_destination)
                job_status = _run_command(ssh_client, command=f'sudo cp {tmp_destination} {dest_path}')
                print(job_status)


def get_connection(config):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        config['hostname'],
        username=config['user'],
        key_filename=config['identityfile'][0]
    )
    return ssh


def install_linux_packages(ssh_client, packages, verbose=False):
    print('====================== installing linux packages ======================', flush=True)
    for dep in packages:
        _run_command(ssh_client,
                     command=f'sudo yum install --refresh -y {dep}',
                     message=f'installing {dep}',
                     verbose=verbose)
    print()


def initialize_user(ssh_client):
    print('========================== initializing user ==========================', flush=True)
    _copy_files(ssh_client,
                files=[
                    ('./bash_aliases', '~/.bash_aliases'),
                    ('./bashrc', '~/.bashrc')
                ])

    _run_command(ssh_client,
                 command='mkdir -p ~/.local/bin',
                 message='creating ~/.local/bin folders')

    print()


def _download_pip(ssh_client, verbose=False):
    _run_command(ssh_client,
                 command='curl -o /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py',
                 message='downloading pip installation',
                 verbose=verbose)


def _install_pip(ssh_client, verbose=False):
    _run_command(ssh_client,
                 command='python3 /tmp/get-pip.py --user',
                 message='installing pip',
                 verbose=verbose)

    
def _delete_pip(ssh_client, verbose=False):
    _run_command(ssh_client,
                 command='rm /tmp/get-pip.py',
                 message='removing pip installation',
                 verbose=verbose)


def install_pip(ssh_client, verbose=False):
    print('=========================== installing pip ============================', flush=True)
    try:
        _download_pip(ssh_client, verbose)
        _install_pip(ssh_client, verbose)
        _delete_pip(ssh_client, verbose)
        
    except Exception as err:
        print('failed')
        print(err)
    finally:
        print()


def _install_pip_packages(ssh_client, packages, verbose=False):
    for package in packages:
        _run_command(
            ssh_client,
            command=f'pip install {package}',
            message=f'installing {package}',
            verbose=verbose
        )


def deploy_mlflow(ssh_client, verbose=False):
    print('========================== deploying mlflow ==========================', flush=True)
    _install_pip_packages(ssh_client,
                          packages=['mlflow', 'boto3', 'psycopg2-binary'],
                          verbose=verbose)

    _run_command(ssh_client,
                 command='mkdir -p ~/.config/systemd/user')
    _run_command(ssh_client,
                 command='mkdir -p ~/.config/mlflow')

    _copy_files(ssh_client,
                files=[
                    ('./mlflow.service', '~/.config/systemd/user/mlflow.service'),
                    ('./credentials/mlflow.env', '~/.config/mlflow/mlflow.env')
                ])

    _copy_files(ssh_client,
                files=[
                    ('./mlflow.conf', '/etc/nginx/conf.d/mlflow.conf')
                ],
                sudo=True)

    _run_command(ssh_client,
                 command='systemctl --user stop mlflow',
                 message='stopping mlflow service')

    _run_command(ssh_client,
                 command='systemctl --user daemon-reload',
                 message='reloading systemctl daemon')

    _run_command(ssh_client,
                 command='systemctl --user enable --now mlflow',
                 message='starting mlflow service')

    _run_command(ssh_client,
                 command='sudo systemctl enable --now nginx',
                 message='starting nginx service')
    print()


def main(config_path, host):
    ssh_config = paramiko.SSHConfig()
    with open(config_path) as f:
        ssh_config.parse(f)

    # Get SSH configuration for specified hostname
    config = ssh_config.lookup(host)

    # Connect to remote host using SSH configuration
    with get_connection(config) as ssh_client:
        # Install vim and tmux on remote host
        install_linux_packages(ssh_client,
                               packages=['vim', 'tmux', 'nginx', 'postgresql15'])
        
        # Setup initial user with .bashrc, and .bash_aliases" "
        initialize_user(ssh_client)
        
        # Download and install pip
        install_pip(ssh_client)
        
        # Install ML FLow and setup
        deploy_mlflow(ssh_client)

    print('finished')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='AWS Provision Tool',
        description='Small tool used to provision AWS EC2 instance for ML Flow',
        epilog='Inspired by curiosity and boredom')

    home = Path.home()
    parser.add_argument('-c', dest='config_path', default=f'{home}/.ssh/config', help='path to the SSH config file')
    parser.add_argument('-H', dest='host', required=True, help='host to connect to (from SSH configuration file)')
    args = parser.parse_args()
    
    main(args.config_path, args.host)
