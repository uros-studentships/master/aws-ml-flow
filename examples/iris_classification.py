import os
import time
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import mlflow


def tic(msg):
    st = time.perf_counter()
    print(f'{msg}...', end='', flush=True)
    
    def toc_fn():
        et = time.perf_counter()
        elapsed_ms = int((et - st)*1000)
        print(f'done [{elapsed_ms}ms]', flush=True)

    return toc_fn


toc = tic('verirying environment')
assert 'AWS_ACCESS_KEY_ID' in os.environ, 'AWS access key id not provided'
assert 'AWS_ACCESS_KEY_ID' in os.environ, 'AWS secret access key not provided'
assert 'AWS_ACCESS_KEY_ID' in os.environ, 'AWS S3 endpoint URL not provided'
toc()

toc = tic('configuring mlflow')
mlflow.set_tracking_uri('http://ec2-23-20-172-128.compute-1.amazonaws.com')
mlflow.set_experiment('iris_logreg')
mlflow.sklearn.autolog(registered_model_name='iris_logreg')
toc()

toc = tic('loading the iris dataset')
X, y = load_iris(as_frame=True, return_X_y=True)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, stratify=y)
toc()

toc = tic('initializing logistic regression model')
model = LogisticRegression(penalty='l1', max_iter=1000, solver='liblinear')
toc()

print('running the experiment')
with mlflow.start_run() as run:
    model.fit(X_train, y_train)

print('finished')
