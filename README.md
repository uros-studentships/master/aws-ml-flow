# ML Flow deployment
This repo contains a tool to deploy ML Flow to an AWS EC2 instance. It connects to the EC2 instance,
installs all dependencies, installs ML Flow tracking server, creates a systemd service to manage the server,
and sets up the nginx as a reverse proxy for the ML Flow server.

## Preparation
Tool expects that the instance and other resources like IAM users and roles, RDS, S3, and VPC are provisioned
correctly. Also, database should also be created manually, although ML Flow will create tables automatically.

This is intended to be used on a fresh EC2 instance, but operation is idempotent so it can be used to update
configs. Prerequisites for running provisioning script is to have a valid SSH config file, and appropriate
`.pem` key like this:
```ssh-config
Host aws
    HostName ec2-xx-xx-xxx-xxx.compute-x.amazonaws.com
    User ec2-user
    IdentityFile ~/.ssh/aws_key.pem
```

## Usage
Running the script from the root folder with a name of the host from SSH config file, and with an optional
parameter which sets a path for the SSH config file:
```sh
$ python ./bin/provision.py -c ~/.ssh/config -H aws
```

Full description available with `--help` flag:
```sh
$ python ./bin/provision.py --help
usage: AWS Provision Tool [-h] [-c CONFIG_PATH] -H HOST

Small tool used to provision AWS EC2 instance for ML Flow

options:
-h, --help      show this help message and exit
-c CONFIG_PATH  path to the SSH config file
-H HOST         host to connect to (from SSH configuration file)

Inspired by curiosity and boredom
```

## Example
There is an example script in the `examples` folder which load the Iris dataset and trains a
logistic regression model. This script will publish the experiment into the configured ML Flow
instance and deploy the model to the S3 bucket which can be used in production.

## Author notes
I'm still not sure why I havn't used Ansible for this, but it was fun automating manually (what an oxymoron :D). 